Con el objetivo de un efectivo entendimiento de la documentacion, la documentacion del Backend y Frontend estaran en Ingles.

- Especificaciones Funcionales

1.  El sistema debe poder realizar un tipo de cambio a un monto donde se deben utilizar el monto, moneda de origen, moneda de destino, monto con el tipo de campo y el tipo de cambio

2.  Debe Permitir el registro, actualización y búsqueda del tipo de cambio

3.  Debe tener una autenticación

4.  Por cada tipo de cambio realizado, se debe registrar quien hizo la solicitud (auditoría funcional)

- Especificaciones No Funcionales

1.  Lenguaje es Java(opcional, puede ser cualquier otro como C# .net)

2.  Framework es spring boot(deseable)

3.  Utilizar programación reactiva RxJava

4.  Utilizar una base de datos como PostgreSQL, MySQL, MS SQL Server dockerizada

5.  Dockerizar los componentes resultantes con una base de SO (CentOS, Ubuntu, etc)

6.  La seguridad debe ser a través de JWT

7.  Usar Postman o SOAPUI para el consumo de la API

8.  Documentar la arquitectura de software

9.  Determinar el análisis de rendimientos para obtener una capacidad de atención de 500 tps (sustentar)

10. Ejecutar una prueba de rendimiento y mostrar sus resultados (SOAPUI o Jmeter)

11. Trabajar en las ramas como si fuera un caso de DevSecOps(con definición de actividades)

- Especificaciones Funcionales adicionales

1.  Manejar roles, perfiles y sesiones.

- Especificaciones No Funcionales adicionales

1.  Implementar un front en angular(o el que mejor maneje) que consuma la API
