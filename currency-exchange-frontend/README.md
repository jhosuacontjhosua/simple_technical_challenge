# Currency-app

## FrontEnd of the currency app

Basic use of the app, by itself:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

### Deploying it using Docker

First run this to build the image(service) using the Dockerfile
```bash
sudo docker -t currency-app .
```

To run the image and create a container called currency-app run this
```bash
sudo docker run -dp 3000:3000 currency-app --name currency-app
```

## Deploying the images using docker-compose

The easiest way to deploy this project is using docker-compose and you can do it by running
the following code on the main directory
```bash
sudo docker-compose up -d
```
