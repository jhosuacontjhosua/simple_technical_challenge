"use client"
import { ChevronDownIcon, CogIcon, PowerIcon, SignalIcon, UserIcon } from '@heroicons/react/20/solid'
import { useState } from 'react'
import SelectSearch from './utils/SelectSearch';

export default function Home() {
    const [isOpen, setIsOpen] = useState(false);
    const [dataResult, setDataResult] = useState(false);

    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    }
    const convertData = () => {
        setDataResult(!dataResult);
    }

    return (
        <main className="h-screen w-screen relative flex flex-col place-items-center before:absolute before:h-[300px] before:w-[480px] before:-translate-x-1/2 before:rounded-full before:bg-gradient-radial before:from-white before:to-transparent before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-[240px] after:translate-x-1/3 after:bg-gradient-conic after:from-sky-200 after:via-blue-200 after:blur-2xl after:content-[''] before:dark:bg-gradient-to-br before:dark:from-transparent before:dark:to-blue-700 before:dark:opacity-10 after:dark:from-sky-900 after:dark:via-[#0141ff] after:dark:opacity-40 before:lg:h-[360px]">

            {/* Superior part of the web(Navbar) */}
            <div className="flex flex-row h-14 w-screen border-b place-items-center justify-between px-10">
                <div>LOGO</div>
                <div className="relative inline-block">
                    <div
                        className="flex flex-row items-center space-x-1 cursor-pointer hover:underline"
                        onClick={toggleDropdown}
                    >
                        <span>Username</span>
                        <ChevronDownIcon className="h-6 w-6" />
                    </div>
                    {isOpen && (
                        <div className="pt-2 absolute right-0 mt-2  w-48  shadow-lg">
                            <ul className='border rounded-md'>
                                <li className="px-4 py-2 hover:bg-gray-100/10 cursor-pointer flex flex-row place-items-center"><UserIcon className='h-6 pr-4' /> Profile</li>
                                <li className="px-4 py-2 hover:bg-gray-100/10 cursor-pointer flex flex-row place-items-center"><PowerIcon className='h-6 pr-4' />Log Out</li>
                                <li className="px-4 py-2 hover:bg-gray-100/10 cursor-pointer flex flex-row place-items-center"><CogIcon className='h-6 pr-4' /> Settings</li>
                            </ul>
                        </div>
                    )}
                </div>
            </div>

            {/* Central part of the web */}
            <div className="w-screen h-auto flex flex-col items-center">
                <h1 className='py-16 text-xl font-medium'>Currency Converter</h1>
                <div className='h-auto w-10/12'>
                    <div className='border rounded-xl h-full w-full flex flex-col px-28'>
                        <div className='flex-1 h-full w-full flex flex-row mt-10'>
                            <div className='flex-auto h-full w-full'>
                                <label class="block">
                                    <span class="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-400">
                                        Amount
                                    </span>
                                    <input type="text" name="amount" class="mt-1 px-2 py-2 bg-black border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block rounded-md sm:text-sm focus:ring-1" placeholder="50" />
                                </label>
                            </div>
                            <div className='flex-auto h-full w-full'>From <SelectSearch /></div>
                            <div className='flex-auto h-full w-full'>To <SelectSearch /></div>
                        </div>
                        <div className='flex-1 w-full flex flex-row-reverse mb-8 pr-10'>
                            <button onClick={convertData} className='rounded-full border my-5 p-2 px-6 hover:bg-white hover:text-black'>Convert</button>
                        </div>
                        <div className='flex-1 bg-white h-80 w-full'>
                            {dataResult &&
                                <div className='h-6'>HELLO</div>
                            }
                        </div>
                    </div>
                </div>
            </div>

        </main>
    )
}
