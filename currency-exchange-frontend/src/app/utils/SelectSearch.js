"use client"
import { useState } from 'react';
import Select from 'react-select';

const SelectSearch = () => {
    const options = [
        { value: 'option1', label: 'Option 1' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option3', label: 'Option 3' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
        { value: 'option2', label: 'Option 2' },
    ];

    const [selectedOption, setSelectedOption] = useState(null);

    const handleSelectChange = (selectedOption) => {
        setSelectedOption(selectedOption);
    };

    const customStyles = {
        control: (provided) => ({
            ...provided,
            backgroundColor: 'black',
            color: 'white',
        }),
        option: (provided, state) => ({
            ...provided,
            backgroundColor: state.isFocused ? '#59595950' : 'black',
            color: 'white'
        }),
        menu: (provided) => ({
            ...provided,
            backgroundColor: 'black',
            color: 'white'
        })
    }

    return (
        <div className='mt-1 pr-10'>
            <Select
                value={selectedOption}
                options={options}
                isSearchable
                onChange={handleSelectChange}
                placeholder="Search..."
                styles={customStyles}
            />
        </div>
    );
};

export default SelectSearch;
